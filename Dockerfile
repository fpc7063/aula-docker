FROM debian:bullseye
LABEL maintainer=francisco.pc7063@gmail.com

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN apt-get update \
    && apt-get install -y bash curl gnupg2 \
    && curl -fsSL https://deb.nodesource.com/setup_18.x | bash - \
    && apt-get install -y nodejs nginx \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /opt/react-app

COPY ./docker-hello-world /opt/react-app

RUN npm install \
    && npm run build \
    && rm -rf /var/www/html/* \
    && mv /opt/react-app/build/* /var/www/html

ENTRYPOINT [ "/bin/bash", "-c" ]
CMD [ "nginx -g 'daemon off;'" ]