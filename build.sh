#!/bin/bash

# Load variables
source RELEASE
FULL_NAME="$REGISTRY/$MAINTAINER/$IMAGE:$MAJOR_VERSION.$MINOR_VERSION"
FULL_NAME_LATEST="$REGISTRY/$MAINTAINER/$IMAGE:$VERSION"


# Build container
docker build . -t $FULL_NAME
docker build . -t $FULL_NAME_LATEST


# Push image to registry
#docker push $FULL_NAME
#docker push $FULL_NAME_LATEST