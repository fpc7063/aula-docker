# Exemplo de utilização de docker para distribuir um frontend

OBS: Aqui o frontend está sendo copiado para o container, isso fará com que uma das layers da imagem contenha o código fonte. \
Para software proprietário o ideal é utilizar um pipeline para criar o build com `npm run build` e então dar *COPY* para o caminho apropriado